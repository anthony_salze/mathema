---
author: Anthony Salze
title: Conditions d'utilisation
hide :
    -navigation
    -toc
    -footer
---
# <span style="color:Tomato"> Conditions d'utilisation
!!! danger "Conditions d'utilisation"
    * Interdit de reposter le jeu sur une autre plate-forme sans mon autorisation.
    * Interdit de vendre le jeu.
    * Le seul lien officiel pour obtenir le jeu se trouve la partie Téléchargements. 
    * Si vous utilisez le jeu, veillez à ne pas vous attribuer les droits de création et indiquer que le jeu a été réalisé par Anthony Salze. Mentionnez ce site internet comme référence.

Toute infraction faite à mes conditions d'utilisation me donnera le droit de vous forcer à arrêter d'utiliser ce jeu.