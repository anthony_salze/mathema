---
author: Anthony Salze
title: Remerciements
hide :
    -navigation
    -toc
    -footer
---
# <span style="color:Tomato"> Remerciements

Le site est hébergé par la forge de [l'*Association des Enseignantes et Enseignants d'Informatique de France*](https://aeif.fr/index.php/accueil/).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Merci à [Mireille Coilhac](https://forge.aeif.fr/mcoilhac) pour son atelier lors des journées nationales de l'APMEP Rennes 2023.
