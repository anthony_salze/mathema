---
author: Anthony Salze
title: Actualités
hide :
    -navigation
    -toc
    -footer
---
# <span style="color:Tomato"> Actualités
!!! info "16 décembre 2023"
    Animation d'un atelier lors de la [Journée de la régionale de Lille de l'APMEP](https://apmeplille.fr/journee-de-la-regionale-de-lille-de-l-apmep.html#). Merci à tous les participants.

!!! info "10 novembre 2023"
    Présentation du jeu lors d'une séance du groupe [EMTA de l'IREM de Lille](https://irem.univ-lille.fr/groupes/emta/).

!!! info "8 mars 2023"
    Présentation du jeu lors du Colloque [Rencontres Didactique et Histoire des Mathématiques - 2ème édition](https://didhim2.sciencesconf.org/) qui s'est déroulé les 8 et 9 mars 2023 à l'Université de Lens.
