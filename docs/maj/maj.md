---
author: Anthony Salze
title: Mises à jour
hide :
    -navigation
    -toc
    -footer
---
# <span style="color:Tomato"> Mises à jour
??? note pliée "Jeu principal"
    ??? note pliée "À venir - Version 0.3.1"
        * Insertion des commandes LaTeX au sein du jeu pour faciliter l'écriture des formules mathématiques (en conception) avec l'aide de MathJax.
        * Le joueur pourra maintenant entrer du texte pour répondre à certaines questions (utile pour les calculs avec les nombres décimaux,...)
        * Suite et fin de la quête principale : Thalès de Milet.
        * Suite quête principale : Pythagore (retour en Grèce antique) ? Numération chinoise (voyage dans les années -300) ?
        * Ajout d'une quête annexe sur la numération minoenne (présent).
        * Ajout d'une quête annexe sur la numération des mayas (présent).
        * Ajout d'une quête annexe sur les différentes bases utilisées en mathématiques (présent).
        * Ajout d'une quête annexe sur la numération égyptienne + système de fractions.
        * Ajout d'un nouveau lieu dans la ville de départ : le musée.
            - Plusieurs salles avec les souvenirs trouvés par le joueur au cours de ses voyages
            - Classement chronologique des découvertes et avancées mathématiques.
        * Ajout de quelques leçons, mathématiciens, cartes à l'encyclopédie.
        * Ajout de quelques succès supplémentaires.
        * Màj du journal de quêtes.
        * Màj de la mini-carte du joueur.
        * Corrections orthographiques.

    ??? note pliée "2023"
        ??? note pliée "Décembre 2023 - Version 0.3.0"
            * Amélioration de la stabilité du jeu
            
        ??? note pliée "Novembre 2023 - Version 0.2.7"
            * Corrections orthographiques
            * Correction d'un bug ne permettant pas l'affichage du Patch Notes
            * Changement du nom de l'exécutable du jeu : Game.exe devient Mathêma.exe
            * Ajout aux téléchargements de la version Linux (version 0.1.0) et de la version Mac (version 0.1.0)

        ??? note pliée "Octobre 2023 - Version 0.2.7"
            * Ajout de nouveaux dialogues au début du jeu et sur le Plateau de Gizeh.
            * Ajout d'une fonctionnalité permettant d'obtenir la définition de certains mots compliqués en passant la souris dessus.
            * Ajout d'une nouvelle option dans le menu principal du jeu : Patch Notes. Ouvre le fichier des mises à jour réalisés sur le jeu.
            * Refonte graphique et technique de la forêt de Milet.
            * Refonte technique du menu Bibliothèque -> remplacé par l'objet Encyclopédie.
            * Ajout d'un système de succès (achievements)
            * Nouvel objet : Encyclopédie. Permet d'accéder aux leçons apprises et à d'autres informations sur l'histoire des mathématiques.
            * Nouvel objet : Sac de cours. Permet d'accéder aux différents succès débloqués dans le jeu.
            * Refonte graphique de quelques cartes.
            * Corrections orthographiques.

        ??? note pliée "Septembre 2023 - Version 0.2.7"
            * Ajout de nouveaux dialogues au début du jeu et sur le Plateau de Gizeh.
            * Ajout d'une fonctionnalité permettant d'obtenir la définition de certains mots compliqués en passant la souris dessus.
            * Ajout d'une nouvelle option dans le menu principal du jeu : Patch Notes. Ouvre le fichier des mises à jour réalisés sur le jeu.
            * Refonte graphique et technique de la forêt de Milet.
            * Refonte technique du menu Bibliothèque -> remplacé par l'objet Encyclopédie.
            * Ajout d'un système de succès (achievements)
            * Nouvel objet : Encyclopédie. Permet d'accéder aux leçons apprises et à d'autres informations sur l'histoire des mathématiques.
            * Nouvel objet : Sac de cours. Permet d'accéder aux différents succès débloqués dans le jeu.
            * Refonte graphique de quelques cartes.
            * Corrections orthographiques.

    ??? note pliée "2021"
        ??? note pliée "Décembre 2021 - Version 0.2.6"
            * Correction bug notepad : toutes les touches du clavier azerty y compris celles du pavé numérique sont prises en compte.
            * Correction bug Uruk : le joueur ne peut plus marcher sur les fenêtres.
            * Correction bug Uruk : le joueur doit désormais lire les tablettes chèvres, moutons et vaches pour pouvoir parler au berger et poursuivre la quête.
            * Suite quête de Thalès (Egypte et mesure de la hauteur de la pyramide : complet)

        ??? note pliée "Juin 2021 - Version 0.2.5"
            * Changement des fichiers de sauvegarde : affichage du nom du héros et du lieu de sauvegarde.
            * Ajout des crédits au menu du jeu.
            * Ajout des sauvegardes automatiques.
            * Pour entrer le nom du joueur et du professeur -> utilisation du clavier directement.
            * Désactivation du clic de souris pour se déplacer.
            * Désactivation de certaines options inutiles : Toujours courir, se rappeler.
            * Nouvel objet : Chaussures de sport (s'obtient au cours de la quête principale).
            * Ajout d'un nouveau menu "Livres" : permet de lire et de conserver certains livres. Dans ces livres seront présentées des notions mathématiques et/ou des éléments de culture sur les peuples visités.
            * Ajout d'un nouveau menu "Carnet de notes" : permet au joueur d'écrire des instructions ou des informations dans une note.
            * Ajout d'un PNJ "Intendante" à l'accueil de l'école de Polis. Permet de rappeler l'utilisation et l'utilité des différentes options du menu.
            * Ajout d'une minimap en haut de l'écran du joueur sur laquelle apparaissent plusieurs informations (entrée, porte, point de sauvegarde, joueur,...).
            * Refonte graphique du village de départ.
            * Correction de certains bugs de texture.

        ??? note pliée "Février 2021 - Version 0.2.4"
            * Suite de la quête de Thalès : 1ère et 2ème missions.
            * Nouvelles cartes : Port de Milet, Champs de Milet, Milet Artisan/Magasin, Milet - Auberge.
            * Baisse du niveau de difficultés des rencontres aléatoires sur la carte de Milet.
            * Correction de certains bugs.   

    ??? note pliée "2020"
        ??? note pliée "Mai 2020 - Version 0.2.3"
            * Nouvelle mission : La grèce antique : Thalès.
            * Correction de certains bugs.

        ??? note pliée "Avril 2020 - Version 0.2.2"
            * Ajout de l'examen sur la Mésopotamie + gestion des récompenses.
            * Ajout de tests de calculs mentaux sur les PC dans l'école de Polis.
            * Nouveau sage : Plato -> Permet de voyager en Grèce.
            * Nouvelles cartes
	            * Accès à la world map dans le monde présent.
                * Montagne Ouréa
                * Maison de Plato

??? tip "Chapitre 01 - La Mésopotamie"
    ??? tip "Courant 2025 - Version 2.2"
        * Insertion des commandes LaTeX au sein du jeu pour faciliter l'écriture des formules mathématiques (en conception) avec l'aide de MathJax.

    ??? tip "Décembre 2023 - Version 1.2"
        * Problème partiellement résolu : freeze de l'écran à des moments aléatoires.
        * Gameplay
            - ajout d'une sauvegarde automatique pour passer de l'an -3300 à l'an -2300.
            - ajout d'un message au début du jeu permettant de plus facilement situer l'action du jeu
        * Corrections bugs
            - entrer et sortir de Larsa avec les chèvres pouvaient causer un crash du jeu.

    ??? tip "Décembre 2023 - Version 1.1"
        * Amélioration de la stabilité du jeu.
        * Corrections bugs
            - portes de l'entrepôt qui entraînaient un crash du jeu.
            - même bug pour la porte de la maison de l'héroïne
            - la maman de l'héroïne apparaissait en double au début du jeu.

    ??? tip "Novembre 2023 - Version 1.0"
        * Adaptation des quêtes de la Mésopotamie du jeu principal en petite aventure
