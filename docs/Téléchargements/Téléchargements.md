---
author: Anthony Salze
title: Téléchargements
hide :
    -navigation
    -toc
    -footer
---
# <span style="color:Tomato"> Téléchargements

!!! warning "Attention"
    Merci de me faire remonter les différents problèmes rencontrés, bugs, fautes d'orthographes, idées,... en m'écrivant soit sur mon adresse académique soit sur l'adresse gmail suivante : [anthonysalze.mathema@gmail.com](mailto:anthonysalze.mathema@gmail.com)

!!! abstract "Téléchargements - Jeu Principal"
    === "Version Windows"
        [Version 0.3.0](https://mega.nz/file/cnYShDIK#OiHqgvFmdoslELAgAhkfSKzwkzaDo6398qoHtNlzo1k){ .md-button target="_blank" rel="noopener" }
    

!!! abstract "Téléchargements - Chapitre 01 - La Mésopotamie"
    === "Version Windows"
        [Version 1.1](https://mega.nz/file/J25hRLBB#hD2vIpFS3LRDTMKgFFXzeSYeLkhQUh2O49Kp3n8GOao){ .md-button target="_blank" rel="noopener" }


!!! tip "Installation Windows"
    * Télécharger le jeu en cliquant sur le bouton associé à votre système d'exploitation.
    * Une fois le dossier téléchargé, l'extraire :
        - clic droit sur le dossier .zip
        - choisir "**Extraire un fichier**"
    * Dans le dossier, double-clic sur le fichier "**Mathêma.exe**"
![](images/tuto_site1.png){ width=20% }
![](images/tuto_site2.png){ width=40% }