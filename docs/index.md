---
author: Anthony Salze
title: Accueil
hide :
    -navigation
    -toc
    -footer
---
# <span style="color:Tomato"> Mathêma : un jeu vidéo sur l'histoire des mathématiques

Mathêma est un jeu vidéo créé à l'aide du logiciel [RPG Maker MV](https://www.rpgmakerweb.com).
C'est un RPG (Role Playing Game) à la 3ème personne, c'est-à-dire que le héros est visible en vue de dessus. Il peut se déplacer et intéragir avec des éléments du décor et d'autres personnages.

Le but de ce jeu est de découvrir un pan de l'histoire des mathématiques.

Il peut être accessible à partir de la classe de CM2.

Voici quelques images :

![](images/05_Uruk_Tablette1.png){ width=30% }
![](images/09_Uruk_partage.png){ width=30% }
![](images/14_Thalès2.png){ width=30% }
![](images/16_Thalès4.png){ width=30% }
![](images/22_Thalès10.png){ width=30% }
![](images/24_Pyramides.png){ width=30% }

Pour faciliter son exploitation au sein d'une classe, le jeu principal sera scindé en plusieurs chapitres portant sur une période ou un personnage historique.


<span style="color:LightSalmon">_Dernière mise à jour : 09/02/2025_
