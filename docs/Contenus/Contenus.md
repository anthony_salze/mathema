---
author: Anthony Salze
title: Contenus
hide :
    -navigation
    -toc
    -footer
---
# <span style="color:Tomato"> Contenus

## <span style="color:LightSalmon"> Histoire des mathématiques

!!! example "Contenus historiques"
    === "La Mésopotamie"
        * Découverte de la notion de calcul par les Sumériens
        * Utilisation de l'écriture cunéiforme sur des tablettes en argile
    
    === "Grèce Antique : Thalès"
        * Propriétés géométriques et théorèmes de Thalès
        * Légende : mesure de la hauteur de la pyramide de Khéops


## <span style="color:LightSalmon"> Notions mathématiques
!!! example "Contenus mathématiques"
    === "Géométrie"
        * Notions d'angle, angles opposés par le sommet, supplémentaires, complémentaires
        * Nature des triangles et des polygones
        * Théorème de Thalès (français, suisse et allemand)
    === "Calculs"
        * Additions, soustractions et multiplications de nombres entiers
        * Vocabulaire des opérations
        * Division euclidienne : quotient et reste
        * Fraction, calculs de quantité
        * Proportionnalité
        * Résolution d'équations

## <span style="color:LightSalmon"> Ajouts futurs
Le jeu est en construction permanente. Il est codé par une seule personne. C'est pourquoi l'ajout de nouveaux contenus ne sera pas régulier.

Pour en savoir plus, je vous invite à visiter l'onglet Mises à jour.