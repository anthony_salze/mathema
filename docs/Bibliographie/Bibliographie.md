---
author: Anthony Salze
title: Bibliographie
hide :
    -navigation
    -toc
    -footer
---
# <span style="color:Tomato"> Bibliographie
!!! info "Sources"
    === "Histoire des mathématiques"
        * Dahan-Dalmedico, A. & Peiffer, J. (mars 1986). *Une histoire des mathématiques. Routes et dédales.* Points.
        * Hauchecorne, B. & Suratteau, D. (2008). *Des mathématiciens de A à Z.* (3ème édition). Ellipses.
        * Houlou-Garcia, A. (avril 2019). *Mathematikos*. Belles Lettres.
        * Collectif Tangente. (2005). *Mille ans d'histoire des mathématiques*. (Bibliothèque Tangente HS n°10). Pole.
        * Collectif Tangente. (2015). *Histoire des mathématiques de l'Antiquité à l'an Mil*. (Bibliothèque Tangente HS n°30). Pole.
        * Barbin, E. (2019). *Faire des mathématiques avec l'histoire au lycée*. Ellipses.
    === "Plugins"
        * SumRndmDde (2020). *Home*. sumrndm. [http://sumrndm.site/](http://sumrndm.site/)
        * Yanfly (2020). *Main Page*. Yanfly.moe Wiki. [http://www.yanfly.moe/wiki/Main_Page](http://www.yanfly.moe/wiki/Main_Page)
        * Galv (2020). *RPG Maker Scripts and Plugins*. Galv's RPG Maker Scripts & Plugins. [https://galvs-scripts.com/](https://galvs-scripts.com/)
        * Darkkitten (2021). *Text Input Window*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/text-input-window.52197/](https://forums.rpgmakerweb.com/index.php?threads/text-input-window.52197/)
        * RPG Maker Irina (2021). *Atelier Irina*. atelieririna.itch.io. [https://atelieririna.itch.io/](https://atelieririna.itch.io/)
        * TheUnproPro (2021). *RPG Maker MV - Minimap w/Fog of War Plugin*. Youtube. [https://www.youtube.com/watch?v=4KQSqXZqa50&t=29s](https://www.youtube.com/watch?v=4KQSqXZqa50&t=29s)
        * AloeGuvner (2021). *Player Notepad (In-Game Text Editor for the Player)*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/player-notepad-in-game-text-editor-for-the-player.91259/](https://forums.rpgmakerweb.com/index.php?threads/player-notepad-in-game-text-editor-for-the-player.91259/)
        * Karberus (2021). *MV Plugins*. Karberus.wordpress. [https://karberus.wordpress.com/category/mv-plugin/](https://karberus.wordpress.com/category/mv-plugin/)
        * KageDesu (2022). *Home*. Pheonix KageDesu plugins. [https://kdworkshop.net/](https://kdworkshop.net/)
        * CasperGaming (2022). *Home*. Casper Gaming Dev Corner. [https://www.caspergaming.com/](https://www.caspergaming.com/)
        * Fallen Angel Olivia (2022). *Home*. Olivia - itch.io. [https://fallenangelolivia.itch.io/](https://fallenangelolivia.itch.io/)
        * Hakuen Studio (2023). *Home*. Hakuen Studio - itch.io. [https://hakuenstudio.itch.io/](https://hakuenstudio.itch.io/)
        * taaspider (2023). *Home*. taaspider - itch.io. [https://taaspider.itch.io/](https://taaspider.itch.io/)
        * TSR : The Northern Frog (2023). *Home*. TSR - itch.io. [https://itch.io/profile/the-northern-frog](https://itch.io/profile/the-northern-frog)
    === "Graphismes"
        * hiddenone (2020). *hiddenone's resource warehouse*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/hiddenones-mv-resource-warehouse.47255/](https://forums.rpgmakerweb.com/index.php?threads/hiddenones-mv-resource-warehouse.47255/)
        * Kal Kally & Kadokawa (2020). *Kal Kally's Generator Resource Collection*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/kal-kallys-generator-resource-collection.117502/](https://forums.rpgmakerweb.com/index.php?threads/kal-kallys-generator-resource-collection.117502/)
        * Avery (2020). *Avy's MV stuff*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/avys-mv-stuff.53317/](https://forums.rpgmakerweb.com/index.php?threads/avys-mv-stuff.53317/)
        * whtdragon (2020). *whtdragon's animals and running horses- now with more dragons!*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/whtdragons-animals-and-running-horses-now-with-more-dragons.53552/](https://forums.rpgmakerweb.com/index.php?threads/whtdragons-animals-and-running-horses-now-with-more-dragons.53552/)
        * TheRealFusion (2020). *Forum*. RPG Maker MV. [https://rpgmaker-mv.de/forum/thread/213-mv-ressourcen-von-therealfusion/](https://rpgmaker-mv.de/forum/thread/213-mv-ressourcen-von-therealfusion/)
        * LadyBaskerville (2020). *RPG Maker XP Graphics for RPG Maker MV (Tilesets and Characters)*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/rpg-maker-xp-graphics-for-rpg-maker-mv-tilesets-and-characters.73430/](https://forums.rpgmakerweb.com/index.php?threads/rpg-maker-xp-graphics-for-rpg-maker-mv-tilesets-and-characters.73430/)
        * Cyanide & Kadokawa (2020). *Cyanides Edits*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/cyanides-edits.51636/](https://forums.rpgmakerweb.com/index.php?threads/cyanides-edits.51636/)
        * Esziaprez (2020). *Esz's rmmv (Others) New ship*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/eszs-rmmv-others-new-ship.73449/](https://forums.rpgmakerweb.com/index.php?threads/eszs-rmmv-others-new-ship.73449/)
        * Alilali & Kadokawa (2020). *Alilali's random MV stuff*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/alilalis-random-mv-stuff.92882/](https://forums.rpgmakerweb.com/index.php?threads/alilalis-random-mv-stuff.92882/)
        * Haydeos & Kadokawa (2020). *Materials by Haydeos!*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/materials-by-haydeos.47400/](https://forums.rpgmakerweb.com/index.php?threads/materials-by-haydeos.47400/)
        * Dragoonwys
        * SumRndmDde, Snowie & Rhino
        * Enterbrain & RyanBram
    === "Musiques &  Sons"
        * Osabisi Sakura (2020). [https://osabisi.sakura.ne.jp/m2/](https://osabisi.sakura.ne.jp/m2/)
        * Nilesh Jatwa (2020). *Instrumental Music - Arabian Desert -UP&DOWN by Nilesh Jatwa 2017(Royalty Free)*. Youtube. [https://www.youtube.com/watch?v=r3FY5wH5Moo](https://www.youtube.com/watch?v=r3FY5wH5Moo)
        * Aaron Krogh (2020). *RPG Music Pack - Free Commercial Use*. RPG Maker. [https://forums.rpgmakerweb.com/index.php?threads/rpg-music-pack-free-commercial-use.926/](https://forums.rpgmakerweb.com/index.php?threads/rpg-music-pack-free-commercial-use.926/)

